<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AddressSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(ImageProductSeeder::class);
        $this->call(PermissionRoleSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(ProductsSeeder::class);
    }
}
