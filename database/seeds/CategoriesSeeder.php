<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('INSERT INTO `categories` (`id`, `name`, `alias`, `parent_id`, `status`, `icon`, `created_at`, `updated_at`, `created_by`) VALUES
	    (1, \'Bán\', \'ban.html\', 0, 2, NULL, \'2018-03-07 17:02:42\', \'2018-03-07 17:02:42\', 1),
	    (2, \'Cho thuê\', \'cho-thue.html\', 0, 2, NULL, \'2018-03-20 16:20:55\', \'2018-03-20 16:20:55\', 1)');
    }
}
