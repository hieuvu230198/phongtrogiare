<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('INSERT INTO `admin` (`id`, `name`, `email`, `password`, `role`, `level`, `status`, `images`, `created_at`, `updated_at`, `remember_token`, `password_reset`) VALUES
	                (4, \'tuan\', \'truongtuan96@gmail.com\', \'$2y$10$uMVeDqbUPf8h0zvn0.31MecBrn4hPjW4iPKPa3bmz/lkB7U2Qa.PO\', 1, 1, 1, NULL, \'2018-03-21 14:13:29\', \'2018-03-21 14:13:29\', NULL, NULL),
	                (6, \'manh\', \'truongmanh93cntt@gmail.com\', \'$2y$10$S5J54T4ckx52j1yZj4L9WOPQGzgm6SJB9MBtUWKBIdBNktWRVi3VS\', 1, 1, 1, NULL, \'2018-03-21 14:20:14\', \'2018-03-21 14:20:14\', \'8rHWtUK0fnFqHhCZvQ4goPm7VTllGTf6NmXm4sTKfYYzmIN20xKW8Lh26KJc\', NULL)');
    }
}
