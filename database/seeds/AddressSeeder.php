<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('INSERT INTO `address` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
                (1, \'DH CNTT\', 2, \'2018-03-23 03:21:20\', \'2018-03-23 03:21:20\'),
                (2, \'ĐH Công Nghiệp\', 2, \'2018-03-23 03:24:58\', \'2018-03-23 03:24:58\'),
                (3, \'DH Nông Lâm\', 2, \'2018-03-23 03:26:09\', \'2018-03-23 03:26:09\'),
                (4, \'DH Khoa Hoc\', 2, \'2018-03-23 03:27:48\', \'2018-03-23 03:27:48\'),
                (5, \'DH Kinh Tế\', 2, \'2018-03-23 03:29:08\', \'2018-03-23 03:29:08\'),
                (6, \'ĐH Y\', 2, \'2018-03-23 03:31:27\', \'2018-03-23 03:31:27\'),
                (7, \'CĐ Y\', 2, \'2018-03-23 03:34:23\', \'2018-03-23 03:34:23\'),
                (8, \'CĐ Kinh Tế\', 2, \'2018-03-23 03:36:21\', \'2018-03-23 03:36:21\')');
    }
}
