<?php

use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('INSERT INTO `permission_role` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
	(10, 1, \'2018-03-20 16:14:50\', \'2018-03-20 16:14:50\'),
	(10, 3, \'2018-03-20 16:14:50\', \'2018-03-20 16:14:50\'),
	(10, 5, \'2018-03-20 16:14:50\', \'2018-03-20 16:14:50\'),
	(10, 6, \'2018-03-20 16:14:50\', \'2018-03-20 16:14:50\')');
    }
}
