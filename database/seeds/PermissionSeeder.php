<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('INSERT INTO `permissions` (`id`, `name`, `permission_for`, `created_at`, `updated_at`) VALUES
	(2, \'edit_cate\', \'cate\', \'2018-03-19 14:18:34\', \'2018-03-19 14:18:34\'),
	(4, \'view_cate\', \'cate\', \'2018-03-19 14:19:10\', \'2018-03-19 14:19:10\'),
	(7, \'del_product\', \'product\', \'2018-03-19 14:20:59\', \'2018-03-19 14:20:59\'),
	(8, \'view_product\', \'product\', \'2018-03-19 14:21:11\', \'2018-03-19 14:21:11\'),
	(9, \'add_user\', \'user\', \'2018-03-19 14:21:30\', \'2018-03-19 14:21:30\'),
	(10, \'edit_user\', \'user\', \'2018-03-19 14:21:40\', \'2018-03-19 14:21:40\'),
	(11, \'del_user\', \'user\', \'2018-03-19 14:21:48\', \'2018-03-19 14:21:48\'),
	(12, \'view_user\', \'user\', \'2018-03-19 14:21:58\', \'2018-03-19 14:21:58\'),
	(13, \'add_cate\', \'cate\', \'2018-03-20 16:16:53\', \'2018-03-20 16:16:53\'),
	(14, \'del_cate\', \'cate\', \'2018-03-20 16:17:11\', \'2018-03-20 16:17:11\'),
	(15, \'add_product\', \'product\', \'2018-03-20 16:17:29\', \'2018-03-20 16:17:29\'),
	(16, \'edit_product\', \'product\', \'2018-03-20 16:17:50\', \'2018-03-20 16:17:50\')');
    }
}
