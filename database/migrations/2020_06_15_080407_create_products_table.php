<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('alias')->nullable();
            $table->integer('cate_id')->nullable();
            $table->integer('address_id')->nullable();
            $table->dateTime('start_day')->nullable();
            $table->dateTime('end_day')->nullable();
            $table->float('price')->nullable();
            $table->integer('dientich')->nullable();
            $table->string('name_own')->nullable();
            $table->string('phone')->nullable();
            $table->string('address_detail')->nullable();
            $table->string('images')->nullable();
            $table->text('mota')->nullable();
            $table->integer('status')->nullable();
            $table->float('thanhtien')->nullable();
            $table->integer('pending');
            $table->text('gmap')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
