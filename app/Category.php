<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
   protected $table="categories";
   protected $fillable=['name','alias','status','icon','created_by'];

   public function parent(){
       return $this->hasMany('App\Category', 'id');
   }
}
