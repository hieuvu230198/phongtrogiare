<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table="permissions";
    protected $fillable = [
        'name', 'permission_for'
    ];
    public function role(){
        return $this->belongsToMany('App\Role');
    }
}
