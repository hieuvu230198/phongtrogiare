<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:admin',
            'password' => 'required|min:6',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Vui Lòng Nhập Tên!',
            'email.required' => 'Vui Lòng Nhập Email!',
            'password.required' => 'Vui Lòng Nhập Mật Khẩu!',
            'email.email' => 'Email Nhập Sai Định Dạng',
            'email.unique' => 'Email Đã Tồn Tại',
        ];
    }

}
