<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'status'=>'required',
            'sltname'=>'required',
            'txticon'=>'nullable|image'
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Tên không dược để trống',
            'status.required'=>'Trạng thái không được để trống',
            'sltname.required'=>'Danh mục cha không được để trống',
            'txticon.image'=>'icon phải là ảnh'
        ];
    }
}
