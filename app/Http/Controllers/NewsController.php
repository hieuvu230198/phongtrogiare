<?php

namespace App\Http\Controllers;

use App\Address;
use App\Category;
use App\image_news;
use App\News;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news=News::orderBy('id','desc')->paginate(10);
        return view('admin.pages.news.list',compact('news'));
    }
    public function getView(){
        $news=News::orderBy('id','desc')->paginate(10);
        return view('admin.pages.news.ajaxlist',compact('news'));
    }
    public function searchListPro(Request $request) {
        $cate=Category::all();
        $address=Address::all();
        $pro=Product::where('name', 'LIKE', "%$request->name_post%")->orderBy('id','desc')->paginate(10);
        return view('admin.pages.product.list',compact('cate','pro','address'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $news= new News();
            $news->name=$request->name;
            $news->mota=$request->mota;
            $news->noidung=$request->noidung;
            if($request->hasFile('imagenews')){
                $files=$request->file('imagenews');
                foreach ($files as $item){
                    $name=time().".".$item->getClientOriginalName();
                    $path = public_path('/images/news/');
                    $item->move($path, $name);
                    $news->images=$name;
                    $news->save();
                    $imagep=new image_news();
                    $imagep->news_id=$news->id;
                    $imagep->image=$name;
                    $imagep->save();
                }
            } else {
                $news->save();
            }
            return $this->getView();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $news=News::findOrFail($request->id);
        return response()->json($news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $news=News::findOrFail($request->news_id);
            $news->name=$request->name;
            $news->mota=$request->mota;
            $news->noidung=$request->noidung;
            $news->update();
            return $this->getView();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $news=News::findOrFail($request->id);
        $image_path = "/images/news/".$news->images;
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        $news->delete();
        return $this->getView();
    }
}
