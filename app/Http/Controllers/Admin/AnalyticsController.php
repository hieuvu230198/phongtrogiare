<?php

namespace App\Http\Controllers\Admin;

use App\Address;
use App\Admin;
use App\Category;
use App\image_product;
use App\Product;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class AnalyticsController extends Controller
{
    public function list_product()
    {
        $cate=Category::all();
        $address=Address::all();
        $pro=Product::orderBy('id','desc')->paginate(12);
         return view('admin.pages.analytics.analytics_product',compact('cate','pro','address'));
    }

    public function list_user()
    {
        $user=Admin::orderBy('id','desc')->paginate(5);
        return view('admin.pages.analytics.analytics_user',compact('user'));
    }
}
