<?php

namespace App\Http\Controllers;
use App\Admin;
use App\Http\Requests\AdminRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function index() {
        if(Auth::guard('admin')->check()){
            if (Auth::user()->level == 3) {
                return redirect('/');
            } else {
                return view('admin.pages.dasboard');
            }
        }
        else{
            return view('login.login');
        }
    }
    public function getRegister() {
        return view('login.register');
    }
    public function postRegister(Request $request) {
        $request->validate( [
            'name' => 'required|max:255|unique:admin',
            'email' => 'required|email|max:255|unique:admin',
            'password' => 'required|min:6',
        ],[
            'name.unique' => 'Tài Khoản Đã Tồn Tại!',
            'name.required' => 'Vui Lòng Nhập Tên Tài Khoản!',
            'email.required' => 'Vui Lòng Nhập Email!',
            'password.required' => 'Vui Lòng Nhập Mật Khẩu!',
            'email.email' => 'Email Nhập Sai Định Dạng',
            'email.unique' => 'Email Đã Tồn Tại',
        ]);
        try{
            $user=new Admin();
            $user->name=$request->name;
            $user->email=$request->email;
            $user->password=Hash::make($request->password);
            $user->role= 1;
            $user->level= 3;
            $user->status= 1;
            $user->save();
            return redirect()->route('login');
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }
    public function getLogin(){
        if(Auth::guard('admin')->check()){
            return redirect()->route('getDasboard');
        }
        else{
            return view('login.login');
        }
    }

    public function postLogin(Request $request){
        $name=$request->name;
        $password=$request->password;
        if ( Auth::guard('admin')->attempt(['name' => $name, 'password' =>$password,'role'=>1,'status'=>1])) {
            return redirect()->route('getDasboard');
        } else if ( Auth::guard('admin')->attempt(['name' => $name, 'password' =>$password,'role'=>2,'status'=>1])) {
            return redirect()->route('getDasboard');
        } else if ( Auth::guard('admin')->attempt(['name' => $name, 'password' =>$password,'role'=>3,'status'=>1])) {
            return redirect('/');
        }
        else{
            echo"loi";
        }
    }

    public function getLogout(){
        Auth::guard('admin')->logout();
        return redirect()->back();
    }

    public function getListView(){
        $user=Admin::orderBy('id','desc')->paginate(5);
        return view('admin.pages.user.list',compact('user'));
    }

    public function getAdmin(){
        $user=Admin::orderBy('id','desc')->paginate(5);
        return view('admin.pages.user.listajax',compact('user'));
    }

    public function addAddAdmin(AdminRequest $request){
        try{
            $user=new Admin();
            $user->name=$request->name;
            $user->email=$request->email;
            $user->password=Hash::make($request->password);
            $user->role=1;
            $user->level=$request->role;
            $user->status=$request->status;
            $user->save();
            return $this->getAdmin();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getEditAdmin(Request $req){
        try{
            $admin=Admin::findOrFail($req->id);
            return response()->json($admin);
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function postEditAdmin(Request $request){
        try{
            $user=Admin::findOrFail($request->id);
            $user->name=$request->name;
            $user->email=$request->email;
            $user->role=1;
            $user->level=$request->role;
            $user->status=$request->status;
            $user->update();
            return $this->getAdmin();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getDelAdmin(Request $request){
        try{
            $user=Admin::findOrFail($request->id);
            $user->delete();
            return $this->getAdmin();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }
}
