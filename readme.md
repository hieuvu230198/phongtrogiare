## Install Laravel

1. Tạo file .env, copy nội dung từ file .env.example
2. Tạo database, sau đó sửa các tham số trong file .env
    -  DB_DATABASE= 'tendatabse'  (vd: DB_DATABASE=phongtrogiare)
    -  DB_USERNAME= 'ten username' (vd: DB_USERNAME=root)
    -  DB_PASSWORD= 'password' (vd: DB_PASSWORD=)
3. Mở Teminal Run Các Lệnh sau:

- composer install
- php artisan key:generate
- composer dump-autoload
- php artisan migrate:refresh --seed


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
