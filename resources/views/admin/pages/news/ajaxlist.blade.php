<table class="table table-bordered">
    <tbody>
    <tr class="text-center">
        <th>STT</th>
        <th>Tên</th>
        <th>Mô Tả</th>
        <th>Ảnh</th>
        <th>Xóa</th>
    </tr>
    <?php $stt=1?>
    @foreach($news as $item)
        <tr>
            <td>{{$stt++}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->mota}}</td>
            <td style="width: 250px;">
                <img src="images/news/{{$item->images}}" class="img-responsive" alt="{{$item->mota}}">
            </td>
            <td style="width: 5%;"><button type="button" class="btn btn-danger button_update_news" value="{{$item->id}}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></td>
            <td style="width: 5%;"><button type="button" class="btn btn-warning button_del_news" value="{{$item->id}}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></td>
        </tr>
    @endforeach
    </tbody>
</table>