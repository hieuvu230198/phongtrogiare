@extends('admin.master')
@section('title',"Quản lí dự án")
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>QUẢN LÝ TIN TỨC</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
                <li class="active">Danh sách tin tức</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Danh sách tin tức</h3>
                        </div>
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('searchListPro') }}">
{{--                            {{ csrf_field() }}--}}
{{--                            <input type="text" placeholder="Nhập tên bài đăng" name="name_post">--}}
{{--                            <button type="submit" class="btn btn-primary">Tìm Kiếm</button>--}}
                            <button type="button" class="btn btn-success" data-toggle="modal" id="modaladdpro">Thêm Tin Tức</button>
                        </form>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="content-table">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr class="text-center">
                                        <th>STT</th>
                                        <th>Tên</th>
                                        <th>Mô Tả</th>
                                        <th>Ảnh</th>
                                        <th>Xóa</th>
                                    </tr>
                                    <?php $stt=1?>
                                   @foreach($news as $item)
                                       <tr>
                                           <td>{{$stt++}}</td>
                                           <td>{{$item->name}}</td>
                                           <td>{{$item->mota}}</td>
                                           <td style="width: 250px;">
                                               <img src="images/news/{{$item->images}}" class="img-responsive" alt="{{$item->mota}}">
                                           </td>
                                           <td style="width: 5%;"><button type="button" class="btn btn-danger button_update_news" value="{{$item->id}}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></td>
                                           <td style="width: 5%;"><button type="button" class="btn btn-warning button_del_news" value="{{$item->id}}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></td>
                                       </tr>
                                       @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix text-center">
                            {{ $news->links() }}
                        </div>
                    </div>
                </div>
                <!-- ./col -->
            </div>

            {{--modal them--}}
            <div class="modal fade" id="modaladdproduct" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Thêm tin tức</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" action="" method="post" id="addpro" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="box-body">
                                   <div class="col-md-6">
                                       <div class="form-group">
                                           <label for="">Tên tin tức</label>
                                           <input type="text" class="form-control"  placeholder="Tên tin tức" name="name">
                                           <input type="hidden" class="form-control" name="id" id="idnews_">
                                       </div>
                                   </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Hình ảnh</label>
                                            <input type="file" class="form-control" name="imagenews[]" id="imageproduct" multiple>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Mô tả</label>
                                            <textarea  class="form-control" name="mota" id="" cols="30" rows="5"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Nội dung</label>
                                            <textarea  class="form-control" name="noidung" id="" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <!-- /.box-body -->
                                        <button type="submit" class="btn btn-danger" id="button_add_news">Thêm</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            {{--{{modal sửa}}--}}
            <div class="modal fade" id="modal_update_news" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Sửa Tin Tức</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" action="" method="post" id="editproject" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label >Tên</label>
                                        <input type="text" class="form-control" name="name" id="news-name-edit">
                                        <input type="hidden" class="form-control" name="news_id" id="news_id">
                                    </div>
                                    <div class="form-group">
                                        <label >Mô tả</label>
                                        <textarea id="news-description-edit" cols="30" rows="5" class="form-control" name="mota" ></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Nội dung</label>
                                        <textarea id="news-content-edit" cols="30" rows="5" class="form-control" name="noidung" ></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Ảnh</label>
                                        <img src="" class="img-responsive" alt="" id="image-new-edit-pre" style="width: 250px;">
                                    </div>
                                </div>

                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary btneditnews">Sửa</button>
                            </form>
                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                        </div>
                    </div>

                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>

@endsection()
