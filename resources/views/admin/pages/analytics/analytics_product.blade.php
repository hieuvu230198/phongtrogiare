@extends('admin.master')
@section('title',"Quản lí dự án")
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>QUẢN LÝ BÀI ĐĂNG</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
                <li class="active">Danh sách bài đăng</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Danh sách bài đăng</h3>
                        </div>
                        <button type="button" class="btn btn-info btn-success mt-3" data-toggle="modal" id="modaladdpro">Xuất Excel</button>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="content-table">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>STT</th>
                                    <th>Name</th>
                                    <th>SĐT</th>
                                    <th>Ngày đăng</th>
                                    <th>Tình trạng</th>
                                    <th>Trạng thái</th>
                                </tr>
                                <?php $stt=1?>
                               @foreach($pro as $item)
                                   <tr>
                                       <td>{{$stt++}}</td>
                                       <td>{{$item->name}}</td>
                                       <td>{{$item->phone}}</td>
                                       <td>{{$item->end_day}}</td>
                                       <td>@if($item->pending==1)
                                               Đã liên hệ
                                               @else
                                               Chưa liên hệ
                                       @endif</td>
                                       <td>@if($item->status==1){{"Hiện tin"}}
                                           @else($item->status==2){{"Ẩn tin"}}
                                           @endif</td>
                                   </tr>
                                   @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix text-center">
                            {{ $pro->links()}}
                        </div>
                    </div>
                </div>
                <!-- ./col -->
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection()
