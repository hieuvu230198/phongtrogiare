@extends('admin.master')
@section('title',"Quản lí user")
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Quản lí tài khoản</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
                <li class="active">Danh sách tài khoản</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Danh sách tài khoản</h3>
                        </div>
                        <button type="button" class="btn btn-info btn-success" id="button_admin" data-toggle="modal">Xuất Excel</button>
                        <!-- /.box-header -->
                       <div class="content-table">
                           <div class="box-body">
                               <table class="table table-bordered">
                                   <tbody>
                                   <tr>
                                       <th>STT</th>
                                       <th>Name</th>
                                       <th>Email</th>
                                       <th>Chức vụ</th>
                                   </tr>
                                   <?php $stt=1?>
                                   @foreach($user as $item)
                                       <tr>
                                           <th>{{$stt++}}</th>
                                           <th>{{$item->name}}</th>
                                           <th>{{$item->email}}</th>
                                           <th>@if($item->role==1){{"admin"}}
                                               @elseif($item->role==2){{"staff"}}
                                               @else
                                                   {{"Publicer"}}

                                               @endif
                                           </th>
                                       </tr>
                                   @endforeach
                                   </tbody>
                               </table>
                           </div>
                       </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $user->links()}}
                        </div>
                    </div>
                </div>
                <!-- ./col -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection()