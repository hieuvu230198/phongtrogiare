<div class="box-body">
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th>STT</th>
            <th>Name</th>
            <th>Email</th>
            <th>Chức vụ</th>
            <th>Sửa</th>
            <th>Xóa</th>
        </tr>
        <?php $stt=1?>
        @foreach($user as $item)
            <tr>
                <th>{{$stt++}}</th>
                <th>{{$item->name}}</th>
                <th>{{$item->email}}</th>
                <th>@if($item->level==1){{"admin"}}
                    @elseif($item->level==2){{"staff"}}
                    @else
                        {{"Publicer"}}

                    @endif
                </th>
                <th><button class="btn btn-warning btn-edit-admin" value="{{ $item->id }}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></th>
                <th><button class="btn btn-danger btn-del-admin" value="{{ $item->id }}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></th>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>