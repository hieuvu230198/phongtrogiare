@extends('admin.master')
@section('title',"Quản lí Permission")
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Quản lí Permission</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
                <li class="active">Danh sách Permission</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Danh sách permission</h3>
                        </div>
                        <button type="button" class="btn btn-info btn-success pull-right" data-toggle="modal" data-target="#permission" id="addpermission" style="margin: 5px 0px">Thêm Permission</button>
                        <!-- /.box-header -->
                        <div class="box-body">
                           <div class="content-table">
                               <table class="table table-bordered">
                                   <tbody>
                                   <tr>
                                       <th>Stt</th>
                                       <th>Name</th>
                                       <th>Permission_for</th>
                                       <th>Edit</th>
                                       <th>Del</th>
                                   </tr>
                                   <?php $stt=1?>
                                   @foreach($per as $item)
                                       <tr>
                                           <th>{{$stt++}}</th>
                                           <th>{{$item->name}}</th>
                                           <th>{{$item->permission_for}}</th>
                                           <th><button class="btn btn-warning btn-edit-per" value="{{ $item->id }}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></th>
                                           <th><button class="btn btn-danger btn-del-per" value="{{ $item->id }}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></th>
                                       </tr>
                                   @endforeach
                                   </tbody>
                               </table>
                           </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                           {{$per->links()}}
                        </div>
                    </div>
                </div>
                <!-- ./col -->
            </div>

            {{--modal them--}}

            <div class="modal fade" id="modaladdper" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Thêm Permission</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" action="" method="post" id="addper">
                                {{csrf_field()}}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="">Name role</label>
                                        <input type="text" class="form-control"   placeholder="Nhập Username" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Permission_For</label>
                                        <input type="text" class="form-control"   placeholder="Nhập" name="name_for">
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary">Thêm</button>
                            </form>
                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

            {{--modal edit per--}}
            <div class="modal fade" id="modaleditper" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Sửa Permission</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" action="" method="post" id="editpermis">
                                {{csrf_field()}}
                                <div class="box-body">
                                    <input type="hidden" id="id_per" name="id">
                                    <div class="form-group">
                                        <label for="">Name role</label>
                                        <input type="text" class="form-control" id="name"  placeholder="Nhập Username" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Permission_For</label>
                                        <input type="text" class="form-control" id="name_for"  placeholder="Nhập" name="name_for">
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary btn-edit-pers" disabled>Sửa</button>
                            </form>
                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection()