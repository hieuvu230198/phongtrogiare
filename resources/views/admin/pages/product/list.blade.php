@extends('admin.master')
@section('title',"Quản lí dự án")
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>QUẢN LÝ BÀI ĐĂNG</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
                <li class="active">Danh sách bài đăng</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Danh sách bài đăng</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <form action="{{route('searchListPro') }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="text" placeholder="Nhập tên bài đăng" name="name_post">
                                    <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                                    <button type="button" class="btn btn-success" id="modaladdpro">Thêm bài đăng</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="content-table">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>STT</th>
                                    <th>Name</th>
                                    <th>SĐT</th>
                                    <th>Ngày đăng</th>
                                    <th>Trạng thái</th>
                                    <th>Tình trạng</th>
                                    <th>Sửa</th>
                                    <th>Xóa</th>
                                </tr>
                                <?php $stt=1?>
                               @foreach($pro as $item)
                                   <tr>
                                       <td>{{$stt++}}</td>
                                       <td>{{$item->name}}</td>
                                       <td>{{$item->phone}}</td>
                                       <td>{{$item->end_day}}</td>
                                       <td>@if($item->pending==1)
                                               Đã thanh toán
                                               @else
                                               Chưa thanh toán
                                       @endif</td>
                                       <td>@if($item->status==1){{"Hiện tin"}}
                                           @else($item->status==2){{"Ẩn tin"}}
                                           @endif</td>
                                       <td><button type="button" class="btn btn-danger button_update_product" value="{{$item->id}}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></td>
                                       <td><button type="button" class="btn btn-warning button_del_product" value="{{$item->id}}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></td>
                                   </tr>
                                   @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix text-center">
                            {{ $pro->links() }}
                        </div>
                    </div>
                </div>
                <!-- ./col -->
            </div>

            {{--modal them--}}

            <div class="modal fade" id="modaladdproduct" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Thêm bài đăng</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" action="" method="post" id="addpro" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="box-body">
                                   <div class="col-md-6">
                                       <div class="form-group">
                                           <label for="">Tên bài đăng</label>
                                           <input type="text" class="form-control"   placeholder="Tên dự án" name="name">
                                       </div>
                                   </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Danh mục</label>
                                            <select name="sltcate" class="form-control">
                                                <option value="">--chọn--</option>
                                                @foreach($cate as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Diện tích</label>
                                            <input type="text" class="form-control" name="dientich" placeholder="Diện tích">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Giá tiền</label>
                                            <input type="text" class="form-control" name="gia" placeholder="Giá">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Số điện thoại</label>
                                            <input type="text" class="form-control" name="sdt" placeholder="sdt">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Họ tên chủ nhà</label>
                                            <input type="text" class="form-control" name="name_own" placeholder="tên">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Bắt đầu</label>
                                            <input type="text" class="form-control" name="starttime" id="datestart" placeholder="Giá">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Kết thúc</label>
                                            <input type="text" class="form-control" name="endtime" id="dateend" placeholder="Giá">
                                        </div>
                                    </div>
                                   <div class="col-md-6">
                                       <div class="form-group">
                                           <label for="">Trạng thái</label>
                                           <select name="status"   class="form-control">
                                               <option value="" >--chọn--</option>
                                               <option value="1">Hoạt động</option>
                                               <option value="2">Không hoạt động</option>
                                           </select>
                                       </div>
                                   </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Khu vực</label>
                                            <select name="diadiem"   class="form-control">
                                                <option value="" >--chọn--</option>
                                                @foreach($address as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Địa chỉ</label>
                                            <input type="text" class="form-control" name="address_detail">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                         <div class="form-group">
                                              <label >Thành tiền(Đv:vnđ)</label>
                                              <input type="text" class="form-control" id="thanhtien" name="thanhtien">
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Tình trạng</label>
                                            <select name="pending"   class="form-control">
                                                <option value="" >--chọn--</option>
                                                <option value="1">Đã cho thuê</option>
                                                <option value="2">Chưa cho thuê</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Gmap</label>
                                            <input type="text" class="form-control" id="gmap" name="gmap" value="">

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label >Icon</label>
                                        <input type="file" class="form-control" name="imageicon" id="imageicon">
                                    </div>
                                    <div class="preview-icon">

                                    </div>
                                    <div class="form-group">
                                        <label >Hình ảnh</label>
                                        <input type="file" class="form-control" name="imageproduct[]" id="imageproduct" multiple>
                                    </div>
                                    <div class="preview-image">

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Mô tả</label>
                                        <textarea  class="form-control" name="mota" id="" cols="30" rows="10"></textarea>
                                    </div>

                                </div>
                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary">Thêm</button>
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>


            {{--{{modal sửa}}--}}
            <div class="modal fade" id="modal_update_pro" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Sửa bài đăng</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" action="" method="post" id="editproject">
                                {{csrf_field()}}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label >Gmap</label>
                                        <input type="text" class="form-control" name="gmap" id="gmap">
                                        <input type="hidden" class="form-control" name="id" id="idpro_">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Trạng thái </label>
                                        <select name="status"   class="form-control" id="status">

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Tình trạng</label>
                                        <select name="pay"   class="form-control" id="pay">
                                        </select>
                                    </div>

                                    </div>

                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary btneditpro">Sửa</button>
                            </form>
                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                        </div>
                    </div>

                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>

@endsection()
