<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">TRANG QUẢN LÝ</li>
<!---------------------------------------------------------QUẢN LÝ TÀI KHOẢN----------------------------------------------------->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Quản lí tài khoản</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{route('getListUser')}}"><i class="fa fa-circle-o"></i>Tài khoản nhân viên</a></li>
                </ul>
            </li>
 <!---------------------------------------------------------QUẢN LÝ DANH MỤC----------------------------------------------------->           
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-paste"></i>
                    <span>Quản lí danh mục</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('getListCate')}}"><i class="fa fa-circle-o"></i>Danh sách danh mục</a></li>
                </ul>
            </li>
 <!---------------------------------------------------------QUẢN LÝ KHU VỰC-----------------------------------------------------> 
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-map-marker"></i>
                    <span>Quản lý khu vực</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('getListAddress')}}"><i class="fa fa-circle-o"></i>Danh sách địa điểm</a></li>
                </ul>
            </li>
 <!---------------------------------------------------------QUẢN LÝ BÀI ĐĂNG----------------------------------------------------->         
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file"></i>
                    <span>Quản lí bài đăng</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('getListPro')}}"><i class="fa fa-circle-o"></i>Danh sách bài đăng</a></li>
                </ul>
            </li>
 <!---------------------------------------------------------QUẢN LÝ TIN TỨC----------------------------------------------------->         
 <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder-open"></i>
                    <span>Quản lí tin tức</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('getListnews')}}"><i class="fa fa-circle-o"></i>Danh sách tin tức</a></li>
                </ul>
            </li>
 <!---------------------------------------------------------THỐNG KÊ-----------------------------------------------------> 
             <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart"></i>
                    <span>Thống kê</span>
                    <span class="pull-right-container"></span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
               
                <ul class="treeview-menu">
                    <li><a href="{{route('analytics-product')}}"><i class="fa fa-circle-o"></i>Danh sách bài đăng</a></li>          
                    <li><a href="{{route('analytics-user')}}"><i class="fa fa-circle-o"></i>Danh sách tài khoản nhân viên</a></li>           
                   	<li><a href="{{route('getListPro')}}"><i class="fa fa-circle-o"></i>Danh sách tài khoản chủ trọ</a></li>
                </ul>
                <ul class="treeview-menu">
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i>Danh sách tài khoản khách hàng</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>