<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; Trần Hữu Quân <a href="https://adminlte.io">ANTY GROUP</a>.</strong> All rights
    reserved.
</footer>