@extends('site.master')
@section('title','Chi Tiết Tin Tức')
@section('content')
    <div class="container">
        <div class="row" id="productMain">
            <div class="col-md-8">
                <div id="mainImage">
                    <img src="/images/news/{{$news['images']}}" alt="" class="img-responsive image_product">
                </div>
                <div class="box">
                    <h2 class="text-center">{{$news['name']}}</h2>
                </div>
                <div class="box" id="details">
                    <h3><font color="blue">Mô Tả: </font></h3>
                    <p>{!!$news->mota !!}</p>
                    <hr>
                    <div class="social">
                        <h4>Nội Dung:</h4>
                        <p>
                            {!!$news->noidung !!}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- *** MENUS AND FILTERS ***_________________________________________________________ -->
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-body">
                        <button class="btn btn-success"> <h3 class="panel-title">Tin Tức Liên Quan</h3></button>
                        <hr>
                        <ul class="nav nav-pills nav-stacked category-menu">
                            @foreach($newsRelated as $item)
                                <li class="cate_product">
                                    <a href="/tin-tuc/chi-tiet/{{ $item->id }}">{{ $item->name }}</a>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
                <!-- *** MENUS AND FILTERS END *** -->
            </div>
        </div>
    </div>
@endsection
@endsection
