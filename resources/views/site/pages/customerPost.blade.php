<div id="dang-bai">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1>ĐĂNG TIN</h1>
        </div>
        <div class="panel-body">
                <form  action="/dang-bai-cus" method="post" enctype="multipart/form-data" id="cuspostform">
                    {{csrf_field()}}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Tên phòng trọ</label>
                            <input type="text" class="form-control"   placeholder="Tên phòng trọ" name="name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Danh mục</label>
                            <select name="sltcate" class="form-control">
                                <option value="">--chọn--</option>
                                @foreach($cate as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Diện tích</label>
                            <input type="text" class="form-control" name="dientich" placeholder="Diện tích">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Giá</label>
                            <input type="text" class="form-control" name="gia" placeholder="Giá">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Số điện thoại</label>
                            <input type="text" class="form-control" name="sdt" placeholder="Số điện thoại">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Họ tên chủ nhà</label>
                            <input type="text" class="form-control" name="name_own" placeholder="Họ và tên">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Bắt đầu</label>
                            <input type="date" class="form-control" name="starttime" id="datestart1" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Kết thúc</label>
                            <input type="date" class="form-control" name="endtime" id="dateend1" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Khu vực</label>
                            <select name="diadiem"   class="form-control">
                                <option value="" >--chọn--</option>
                                @foreach($address as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Địa chỉ</label>
                            <input type="text" class="form-control" name="address_detail">
                        </div>
                    </div>
<!--
                        <div class="form-group">
                               <label >Giá thuê</label>
                               <input type="text" class="form-control" id="thanhtien" name="thanhtien" value="0" disabled>
                               <input type="hidden" class="form-control" id="thanhtien1" name="thanhtien1">
                        </div>
-->
                    <div class="clear-fix"></div>
                        <div class="form-group">
                            <label >Hình Ảnh</label>
                            <input type="file" class="form-control" name="imageicon" id="imageicon1">
                        </div>
                        <div class="preview-icon1">
                        </div>
<!--
                    <div class="form-group">
                        <label >Hình ảnh</label>
                        <input type="file" class="form-control" name="imageproduct[]" id="imageproduct1" multiple>
                    </div>
-->
                    <div class="preview-image1">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mô tả</label>
                        <textarea  class="form-control" name="mota"></textarea>
                    </div>
                    <!-- /.box-body -->
                    <button type="submit" class="btn btn-primary" >Tiếp tục</button>
                </form>
        </div>
    </div>
</div>
<div class="thanhtoan">
    <div class="col-md-9 col-md-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Quý khách vui lòng thanh toán:</div>
            <div class="panel-body">
                <ul>
                    <li>Số tiền mà quý khách cần phải thanh toán là: <b>100.000 VNĐ</b></li>
                    <li><b>STK:</b> 19035667224011 - <font color="red"><b>TECHCOMBANK</b></font> - TRẦN HỮU QUÂN</li>
                    <li><font color="#f704b5">MOMO</font> - <font color="#2908ed">AIRPAY</font>: <b>0961108598</b></li>
                    <li>Nội dung bài đăng phải tuân thủ nội quy của hệ thống</li>
                    <li>Liên hệ hỗ trợ: 0333.6789.98</li>
                </ul>
                <button class="btn btn-warning btn-success">OK</button>
            </div>
        </div>
    </div>
</div>
<div class="thanks">
    <h3>Bài viết của bạn sẽ được duyệt trong thời gian sớm nhất. Xin chân thành cảm ơn !</h3>
</div>
<script>
    tinymce.init({
        selector:'textarea',
        plugins: ['lists','link'],
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });
</script>
<script type="text/javascript">
    $("#imageproduct1").on('change',function (e) {
        var files=e.target.files;
        filesLength = files.length;
        for (var i = 0; i < filesLength; i++) {
            var f = files[i]
            var fileReader = new FileReader();
            fileReader.onload = (function(e) {
                var file = e.target;
                var temp='<span class="list"><img src="'+e.target.result+'" alt="" width="80px"><span class="remove"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></span></span>';
                $(".preview-image1").append(temp);

                $(".remove").click(function(){
                    $(this).parent(".list").remove();
                });

            });
            fileReader.readAsDataURL(f);
        }
    });
    $.validator.addMethod("money", function(value, element) {
            var isValidMoney = /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:,\d+)?$/.test(value);
            return this.optional(element) || isValidMoney;
        },
        "Gia thue không đúng định dạng (0 or 0.000)"
    );
    $("#cuspostform").validate({
//specify the validation rules
        rules: {
            name: "required",
            mota: "required",
            sltcate: "required",
            diadiem: "required",
            address_detail: "required",
            name_own:"required",
            sdt: {
                required:true,
                number:true,
                minlength:6,
                maxlength:11
            },
            // thoigian:"required",
            gia:{
                required:true,
                money:true,
                minlength:3,
                maxlength:4
            },
            dientich: {
                required:true,
                number:true
            },
            imageicon:{
                required: true,
            },
        },
//specify validation error messages
        messages: {
            name: "Tên không được để trống",
            diadiem: "Khu vực không được để trống",
            address_detail: "Địa chỉ không được để trống",
            name_own: "Tên chủ nhà không được để trống",
            mota: "Mô tả không được để trống",
            sltcate:"Danh mục cha không được để trống",

            sdt:{
                required:"Số điện thoại không được để trống",
                number:"Số điện thoại phải là số",
                minlength:"Số điện thoại quá ngắn",
                maxlength:"Số điện thoại quá dài"
            },
            gia:{
                required:"giá không được để trống",
                money:"giá không đúng định dạng",
                minlength:"giá quá ngắn",
                maxlength:"giá quá dài"
            },
            dientich:{
                required:"Diện tích không được để trống",
                number:"Diện tích phải là số"
            },
            imageicon:{
                required: "icon không được để trống",

            },
        },
        submitHandler: function(form) {
                var form_data = new FormData($("#cuspostform")[0]);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '/dang-bai-cus',
                    type: 'post',
                    data: form_data,
                    dataType: 'json',
                    // async: false,
                    processData: false,
                    contentType: false,
                }).done(function (data) {
                    if(data.message){
                        $(".thanhtoan").show();
                        $("#dang-bai").hide();
                    }
                }).fail(function (data) {
                    console.log(data);
                    // var response = JSON.parse(data.responseText);
                    // printErrorMsg(response.errors);
                });
        }
    });


</script>