@extends('site.master')
@section('title','Trang chủ')
@section('content')

    <div id="content">
        {{--<div id="all">--}}
        <div id="hot">

            <div class="box">
                <div class="container">
                    <div class="col-md-12">
                        <h2>Tin Tức</h2>
                    </div>
                </div>
            </div>
            <div class="container">
                @foreach($news as $item)
                    <div class="col-md-4 news_item">
                        <img class="img-circle" src="/images/news/{{ $item->images }}" alt="Generic placeholder image" width="140" height="140">
                        <h2>{{ $item->name }}</h2>
                        <p class="text-wrap" style="word-break: break-all;">{{ $item->mota }}</p>
                        <p><a class="btn btn-default" href="/tin-tuc/chi-tiet/{{ $item->id }}" role="button">Chi Tiết »</a></p>
                    </div>
                @endforeach
            </div>
            <!-- /.container -->
        </div>
    {{--</div>--}}
    <!-- /#content -->
    </div>
@endsection