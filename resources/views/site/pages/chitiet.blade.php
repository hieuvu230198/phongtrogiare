<div class="content-detail">
 <div class="row" id="productMain">
        <div class="col-sm-6">
            <div id="mainImage">
                <img src="/images/icons/{{$pro->images}}" alt="" class="img-responsive image_product">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box">
                <h2 class="text-center">{{$pro->name}}</h2>
                <h3><font color="red"><b>Khu vực:</b></font> {{$pro->address->name}}</h3>
                <p><b>Địa chỉ:</b> {{$pro->address->name}}</p>
                <p><b>Giá:</b> {{number_format($pro->price, 3)}} VNĐ</p>
                <p><b>Diện tích:</b> {{$pro->dientich}} m<sup>2</sup></p>

               <h3><font color="blue">Thông tin liên hệ:</font></h3>
              <p> Chủ phòng: {{$pro->name_own}}</p>
                <p> Số điện thoại:{{$pro->phone}}</p>
                <h4><font color="blue">Trạng thái:</font> Chưa cho thuê</h4>
               
            </div>

            <div class="row" id="thumbs">
                <div class="col-xs-4">
                    <a href="/images/icons/{{$pro->images}}" class="thumb">
                        <img src="/images/icons/{{$pro->images}}" alt="" class="img-responsive">
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="/images/icons/{{$pro->images}}" class="thumb">
                        <img src="/images/icons/{{$pro->images}}" alt="" class="img-responsive">
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="/images/icons/{{$pro->images}}" class="thumb">
                        <img src="/images/icons/{{$pro->images}}" alt="" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>

                </div>

                <div class="box" id="details">
                    <p>
                    <h3><font color="blue">Chi tiết</font></h3>
                    <p>{!!$pro->mota !!}</p>

                    <hr>
                    <div class="social">
                        <h4>Chia sẻ thông tin</h4>
                        <p>
                            <a href="https://www.facebook.com/Ph%C3%B2ng-Tr%E1%BB%8D-Thanh-Xu%C3%A2n-110305620748092" target="_blank" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                        </p>
                    </div>
                    <div class="map">
                        <iframe src="{{$pro->gmap}}" width="800" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
             </div>

</div>
