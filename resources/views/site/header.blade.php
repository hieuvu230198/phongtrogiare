<div class="navbar navbar-default yamm" role="navigation" id="navbar">
    <div class="container">
        <ul class="nav navbar-nav">
            <li class="active"><a href="/">Trang chủ</a>
            </li>
            @if(Auth::guard('admin')->check())
            <li>
                <a href="dang-bai" class="dangbai">Đăng bài</a>
            </li>
            @endif
            <li>
                <a href="https://www.facebook.com/shopdientucu" class="dangbai">Mua Bán</a>
            </li>
            <li>
                <a href="{{ route('tin-tuc') }}" class="dang-bai">Tin tức</a>
            </li>
            <li>
                <a href="https://www.youtube.com/watch?v=JHGED0el9a8" class="dangbai">Hướng dẫn</a>
            </li>
            <li>
                <a href="" class="dangbai">Liên hệ</a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            @if(Auth::guard('admin')->check())
                <li class="nav-item"><a class="nav-link" href="#">{{ Auth::guard('admin')->user()->name }}</a></li>
             <li class="nav-item"><a class="nav-link" href="{{ route('getLogout') }}">Đăng Xuất</a></li>
            @else
                <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Đăng Nhập</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Đăng Ký</a></li>
            @endif
        </ul>
    </div>
    <!-- /.container -->
</div>
<!-- *** NAVBAR END *** -->
<div class="container-fluid">
    <div class="row">
    <div class="col-md-12">
        <div id="main-slider">
            <div class="item">
                <img src="images/icons/pt3.png"  alt="" class="img-responsive">
            </div>
            <div class="item">
                <img class="img-responsive" src="images/icons/pt2.png" alt="">
            </div>
            <div class="item">
                <img class="img-responsive" src="images/products/3.jpg" alt="">
            </div>
            {{--<div class="item">--}}
                {{--<img class="img-responsive" src="site/img/main-slider4.jpg" alt="">--}}
            {{--</div>--}}
        </div>
        <!-- /#main-slider -->
    </div>
    </div>
</div>
