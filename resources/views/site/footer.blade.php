<div id="footer" data-animate="fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <h4>PHONGTROTHANHXUAN.COM</h4>

                <ul>
                    <li><a href="text.html"><b>Hoteline:</b>0333.6789.98 - 0822.6789.98</a>
                    </li>
                    <li><a href="faq.html"><b>Gmail:</b> antygroupmarketting@gmail.com</a>
                    </li>
                    <li><a href="faq.html"><b>Địa chỉ:</b> 16/62/42 Triều Khúc - Thanh Xuân</a>
                    </li>
                </ul>
                <h4>THANH TOÁN</h4>

                <ul>
                    <li><a href="text.html">TECHCOMBANK</a>
                    </li>
                    <li><a href="text.html">Chi nhánh Thánh Xuân</a>
                    </li>
                    <li><a href="faq.html"><b>STK:</b> 19035667224011</a>
                    </li>
                    <li><a href="faq.html"><b>TRẦN HỮU QUÂN</b></a>
                    </li>
                </ul>
            </div>
            <!-- /.col-md-3 -->

            <div class="col-md-3 col-sm-6">
            <h4>DANH MỤC HỖ TRỢ</h4>
            <ul>
                    <li><a href="text.html"><b>Quảng cáo</b></a>
                    </li>
                    <li><a href="text.html"><b>Chương trình - Khuyến mãi</b></a>
                    </li>
                    <li><a href="faq.html"><b>Quy chế hoạt động</b></a>
                    </li>
                    <li><a href="faq.html"><b>Góc tin tức</b></a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.col-md-3 -->

            <div class="col-md-3 col-sm-6">
            <h4>QUY ĐỊNH</h4>
            <ul>
                    <li><a href="text.html">Quy định đăng tin</a>
                    </li>
                    <li><a href="text.html">Quy chế hoạt động</a>
                    </li>
                    <li><a href="faq.html">Điều khoản thỏa thuận</a>
                    </li>
                    <li><a href="faq.html">Chính sách bảo mật</a>
                    </li>
                    <li><a href="faq.html">Giải quyết khiểu nại</a>
                    </li>
                </ul>

            </div>
            <!-- /.col-md-3 -->



            <div class="col-md-3 col-sm-6">
            <h4>HƯỚNG DẪN</h4>
            <ul>
                    <li><a href="text.html"><b>Báo giá & Hỗ trợ</b></a>
                    </li>
                    <li><a href="text.html"><b>Câu hỏi thường gặp</a>
                    </li>
                    <li><a href="faq.html"><b>Về chúng tôi</b></a>
                    </li>
                    <li><a href="faq.html"><b>Hướng dẫn đăng bài</b></a>
                    </li>
                    <li><a href="faq.html"><b>Hướng dẫn thanh toán</b></a>
                    </li>
                </ul>
<!--
            <script>
            jQuery(document).ready(function() {
            var offset = 220;
            var duration = 500;
            jQuery(window).scroll(function() {
            if (jQuery(this).scrollTop() > offset) {
            jQuery('.back-to-top').fadeIn(duration);} else {
            jQuery('.back-to-top').fadeOut(duration);}});
            jQuery('.back-to-top').click(function(event) {
            event.preventDefault();
            jQuery('html, body').animate({scrollTop: 0}, duration);
            return false;})});</script>
            <a href="#" class=""><img src="images/icons/.png" width="40px" height="40px" alt="Back to Top" /></a>-->
            </div>
            <!-- /.col-md-3 -->
       
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
 
</div>

<div id="copyright">
    <div class="container">
     

            <p class="pull-left"> <center>Copyright @ Trần Hữu Quân - ANTYGROUP</center></p>
       
      
    </div>
</div>